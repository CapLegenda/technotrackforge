//
//  moving_object.cpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 19.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#include "moving_object.hpp"

moving_object::moving_object(
        std::string path_name,
        double x,
        double y,
        double width,
        double height,
        int frames,
        double dx,
        double dy) :
        anim_object(path_name, x, y, width, height, frames),
        dx(dx), dy(dy) {

}

moving_object::moving_object(const moving_object &rhs) : anim_object(rhs) {
    dx = rhs.dx;
    dy = rhs.dy;
}

moving_object moving_object::operator=(const moving_object &rhs) {
    anim_object::operator=(rhs);
    dx = rhs.dx;
    dy = rhs.dy;
    return *this;
}

double &moving_object::get_x() {
    return x;
}

double &moving_object::get_y() {
    return y;
}

double &moving_object::get_dx() {
    return dx;
}

double &moving_object::get_dy() {
    return dy;
}

double moving_object::get_dy() const {
    return dy;
}

double moving_object::get_dx() const {
    return dx;
}


