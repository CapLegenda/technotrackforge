//
//  TApp.cpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 11.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#include <iostream>
#include <sstream>
#include "TApp.hpp"
#include "ResourcePath.hpp"
#include "anim_object.hpp"
#include "moving_object.hpp"
#include "static_object.hpp"

#define kuznya_x 575
#define kuznya_y 320
enum states {
    state_forge,
    state_upgrade,
    state_fight,
};

TApp::TApp():
    Window(nullptr) {
      
}

TApp::~TApp() {
  
}

void TApp::Init() {
    
}
int cvet = 1;
int gold = 0;
double start_upg = 100;
double upgrade = 0;
int boss_gold = 50;
double up = 0;
double boss_health = 100;
double start_health = 100;
double firebal_dmg = 20;
int gold_upg = 50;
bool animation_of_hero = false;
bool fireball_pushed = false;
bool animation_of_kuznya = false;

void update_anim(anim_object& object, const double& time) {
    object.get_current_frame() += 0.01 * time;
    if (object.get_current_frame() > object.get_number_of_frames()) {
        object.get_current_frame() = 0;
    }
    object.get_sprite().setTextureRect(sf::IntRect(object.get_width() * int(object.get_current_frame()), 0, object.get_width(), object.get_height()));
}

void update_moving_object(moving_object& object, const double& time) {
    update_anim(object, time);
    object.get_x() += object.get_dx() * time;
    object.get_y() += object.get_dy() * time;
    object.get_sprite().setPosition(object.get_x(), object.get_y());
    if (object.get_x() > 1200) {
        object.get_x() = 0;
    }
    if (object.get_y() > 500) {
        object.get_y() = 0;
    }
}

void update_fireball_list(std::vector<moving_object> &fireball_list, const double &time, static_object& monster, static_object& health) {
    for (int i = 0; i < 6; ++i) {
        if (!fireball_list[i].is_dead()) {
            update_moving_object(fireball_list[i], time);
            if (fireball_list[i].get_x() > monster.get_x()) {
                std::cout << "ПОПАДАНИЕ ЗАФИКСИРОВАНО." << std::endl;
                boss_health -= firebal_dmg;
                double bub = boss_health/start_health * 600;
                health = static_object("healthbezcherepov-2.png", 0, 0, bub, 300);
                if (boss_health <= 0) {
                    start_health *= 2;
                    boss_health = start_health;
                    gold += boss_gold;
                }
                monster.get_sprite().setColor(sf::Color(255,0,0,127));
                fireball_list[i].die();
            }
        }
        if (fireball_list[i].get_x() > 1000) {
            fireball_list[i].die();
        }
    }
}

void draw_fireballs(std::vector<moving_object> &fireball_list, sf::RenderWindow &window, const double &time) {
    for (int i = 0; i < 6; ++i) {
        if (!fireball_list[i].is_dead()) {
            window.draw(fireball_list[i].get_sprite());
        }
    }
}

void add_fireball(std::vector<moving_object> &fireball_list, int color) {
    for (int i = 0; i < 6; ++i) {
        if (fireball_list[i].is_dead()) {
            switch (color) {
                case 1: {
                    fireball_list[i] = moving_object("fireball_fire.png", 200, 400, 80, 70, 7, 0.5, 0);
                    break;
                }
                case 2: {
                    fireball_list[i] = moving_object("fireball_pink.png", 200, 400, 80, 70, 7, 0.5, 0);
                    break;
                }
                case 3: {
                    fireball_list[i] = moving_object("fireball_green.png", 200, 400, 80, 70, 7, 0.5, 0);
                    break;
                }

            }
            fireball_list[i].live();
            break;
        }
    }
}

void draw_hero(anim_object &hero, anim_object &sheath, anim_object &blade, anim_object &effect, sf::RenderWindow &window) {
    if (!hero.is_dead()) {
        window.draw(hero.get_sprite());
        window.draw(sheath.get_sprite());
        window.draw(blade.get_sprite());
        window.draw(effect.get_sprite());
    }
}

void update_hero(anim_object &hero, anim_object &effect, anim_object &blade, anim_object &sheath, sf::RenderWindow &window, const double time, std::vector<moving_object> &fireball_list) {
    update_anim(hero, time);
    update_anim(effect, time);
    update_anim(blade, time);
    update_anim(sheath, time);
    if ((int)hero.get_current_frame() == 1 && !fireball_pushed) {
        add_fireball(fireball_list, cvet);
        fireball_pushed = true;
    }
    if ((int)hero.get_current_frame() == 2) {
        effect.get_current_frame() = 0;
        hero.get_current_frame() = 0;
        blade.get_current_frame() = 0;
        sheath.get_current_frame() = 0;
        animation_of_hero = false;
        fireball_pushed = false;

    }
}

void update_kuznya(anim_object &blacksmith, anim_object &hammer, anim_object &sparks, anim_object &anvil, sf::RenderWindow &window, const double time) {
    update_anim(blacksmith, time);
    update_anim(hammer, time);
    update_anim(sparks, time);
    update_anim(anvil, time);
    if ((int)blacksmith.get_current_frame() == 4) {
        hammer.get_current_frame() = 0;
        sparks.get_current_frame() = 0;
        anvil.get_current_frame() = 0;
        blacksmith.get_current_frame() = 0;
        upgrade += 10;
        if (upgrade >= start_upg) {
            upgrade = 0;
            up += 1;
        }
        animation_of_kuznya = false;
    }
}

void draw_kuznya(anim_object &blacksmith, anim_object &hammer, anim_object &sparks, anim_object &anvil, sf::RenderWindow &window) {
    if (!blacksmith.is_dead()) {
        window.draw(blacksmith.get_sprite());
        window.draw(hammer.get_sprite());
        window.draw(anvil.get_sprite());
        window.draw(sparks.get_sprite());

    }
}



void TApp::Run() {
    sf::RenderWindow window(sf::VideoMode(1200, 720), "game");
    moving_object fireball("fireball_fire.png", 200,400, 170, 135, 7, 0.5, 0);

    std::vector<moving_object> fireball_list(6, fireball);

    anim_object blacksmith("kuznec_body.png", kuznya_x, kuznya_y, 100, 150, 5);
    blacksmith.live();
    anim_object hammer("molot_steel.png", kuznya_x, kuznya_y, 100, 150, 5);
    anim_object sparks("sparks_default.png", kuznya_x, kuznya_y, 100, 150, 5);
    anim_object anvil("anvil_steel.png", kuznya_x, kuznya_y, 100, 150, 5);

    
    static_object upgrade_ham("vsratiy_obsidian.png", 800, 400, 50, 50);
    static_object upgrade_sword("vsratiy_obsidian.png", 800, 520, 50, 50);
    static_object upgrade_random("vsratiy_obsidian.png", 800, 630, 50, 50);



    static_object fight_background("fight_background.png", 0,0, 1200, 720); // инициализация файт-фона
    static_object main_background("mainlocation2.png", 0,0, 1200, 720);
    static_object upgrade_background("menyu.png", 0, 0, 1200, 720);
    static_object monster("monster.png", 900,200,270,460); // Инициализация монстра
    //monster.live(); // По дефолту все инициализированные объекты - мертвы нахуй
    anim_object hero1("hero_anim.png", 100,400, 600,210, 3); // Скин героя
    hero1.live();
    anim_object sheath("sheath_cream.png", 100, 400, 600, 210, 3); // Скин ножн
    anim_object blade("blade_steel.png", 100, 400, 600, 210, 3); // Клинок
    anim_object effect("effect_pink.png", 100, 400, 600, 210, 3); // Эффект атаки
    static_object upg_ic("ebalo.png", 0,0, 70, 70); // инициализация файт-фона
    static_object fgt_ic("ebalo.png", 1100,0, 200,200);
    static_object frg_ic("ebalo.png", 0,0,200,200);
    static_object health_zeb("healthbezcherepov-2.png", 0, 0, 600, 300);
    static_object health_bez("healthbezcherepov_empty.png", 0, 0, 600, 300);
    static_object bitcoin("bitcoin_70_70.png", 1100, 190, 70, 70);
    static_object up_grade_emp("healthbezcherepov_empty.png", 300, -70, 600, 300);
    static_object up_grade("shkala_sinyaa.png", 300, -70, 0, 0);


    sf::Font font;
    if (!font.loadFromFile(resourcePath() + "Noteworthy.ttc")) {
        return EXIT_FAILURE;
    }
    
    sf::Text text("", font, 40);
    text.setFillColor(sf::Color(255,215,0));
    
    

    anim_object kuznya("kuznya_animation.png",200,200,180,155,3);
    states state = state_forge;

    sf::Clock clock;
    while (window.isOpen()) {
        double time = clock.getElapsedTime().asMicroseconds();
        clock.restart();
        time = time/800;
        sf::Event event;
        //fireball.set_speed(1.7);
        //fireball.set_direction(RIGHT);
        switch (state) {
            case state_forge:
            {
                while (window.pollEvent(event)) {
                    if (event.type == sf::Event::Closed)
                        window.close();
                    if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                        // transform the mouse position from window coordinates to world coordinates
                        sf::Vector2f mouse = window.mapPixelToCoords(sf::Mouse::getPosition());

                        // retrieve the bounding box of the sprite
                        sf::FloatRect bounds = anvil.get_sprite().getGlobalBounds();

                        // hit test
                        if (bounds.contains(mouse))
                        {
                            animation_of_kuznya = true;
                        }

                        bounds = fgt_ic.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse)) {
                            state = state_fight;
                        }
                        
                        bounds = frg_ic.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse)) {
                            state = state_upgrade;
                        }
                    }
                }

                
                if (animation_of_kuznya) {
                    update_kuznya(blacksmith, hammer, sparks, anvil, window, time);
                    up_grade = static_object("shkala_sinyaa.png", 300, -70, upgrade/start_upg * 600, 300);
                }

                if (up == 1) {
                    hammer = anim_object("molot_diamond.png", kuznya_x, kuznya_y, 100, 150, 5);
                    anvil = anim_object("anvil_dark.png", kuznya_x, kuznya_y, 100, 150, 5);
                    up += 1;
                }
                if (up == 3) {
                    blade = anim_object("blade_obsidian.png", 100, 400, 600, 210, 3);
                    start_health/= 2;
                }
                if (up == 4) {
                    blade = anim_object("blade_diamong.png", 100, 400, 600, 210, 3);
                    effect = anim_object("effect_fire.png", 100, 400, 600, 210, 3);
                    start_health/= 2;
                }
                if (up == 5) {
                    cvet = 2;
                    firebal_dmg = 40;
                }
                if (up == 6) {
                    cvet = 3;
                    firebal_dmg = 60;
                }


                window.clear();
                window.draw(main_background.get_sprite());
                window.draw(fgt_ic.get_sprite());
                window.draw(frg_ic.get_sprite());
                window.draw(up_grade_emp.get_sprite());
                window.draw(up_grade.get_sprite());
                // window.draw(fireball.get_sprite());
                // window.draw(kuznya.get_sprite());

                draw_kuznya(blacksmith, hammer, sparks, anvil, window);
                std::ostringstream forge_score_str;
                forge_score_str << gold;
                text.setString("" + forge_score_str.str());
                text.setPosition(1010, 200);
                window.draw(text);
                window.draw(bitcoin.get_sprite());

                window.display();
                break;
            }
            case state_upgrade:
            {
                while (window.pollEvent(event)) {
                    if (event.type == sf::Event::Closed)
                        window.close();
                    if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                        // transform the mouse position from window coordinates to world coordinates
                        sf::Vector2f mouse = window.mapPixelToCoords(sf::Mouse::getPosition());

                        // retrieve the bounding box of the sprite
                        sf::FloatRect bounds = hero1.get_sprite().getGlobalBounds();

                        // hit test
                        if (bounds.contains(mouse))
                        {
                        }

                        bounds = fgt_ic.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse)) {
                            state = state_forge;
                        }

                        bounds = upgrade_sword.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse) && gold >= gold_upg) {
                            gold -= gold_upg;
                            upgrade_sword = static_object("vsratiy_diamond.png", 800, 520, 50, 50);
                        }
                        
                        bounds = upgrade_ham.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse) && gold >= gold_upg) {
                            gold -= gold_upg;
                            upgrade_ham = static_object("vsratiy_diamond.png", 800, 400, 50, 50);
                        }

                        bounds = upgrade_random.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse) && gold >= gold_upg) {
                            gold -= gold_upg;
                            upgrade_random = static_object("vsratiy_diamond.png", 800, 400, 50, 50);
                        }
                    }
                }

                


                window.clear();
                window.draw(upgrade_background.get_sprite());
                window.draw(fgt_ic.get_sprite());
                window.draw(upgrade_ham.get_sprite());
                window.draw(upgrade_sword.get_sprite());
                window.draw(upgrade_random.get_sprite());
                std::ostringstream forge_score_str;
                forge_score_str << gold;
                text.setString("" + forge_score_str.str());
                text.setPosition(1020, 0);
                window.draw(text);


                // window.draw(fireball.get_sprite());

                //draw_kuznya(blacksmith, hammer, sparks, anvil, window);

                window.display();
                break;
            }
            case state_fight:
            {
                while (window.pollEvent(event)) {
                    if (event.type == sf::Event::Closed)
                        window.close();
                    if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                        // transform the mouse position from window coordinates to world coordinates
                        sf::Vector2f mouse = window.mapPixelToCoords(sf::Mouse::getPosition());

                        // retrieve the bounding box of the sprite
                        sf::FloatRect bounds = hero1.get_sprite().getGlobalBounds();

                        // hit test
                        if (bounds.contains(mouse))
                        {
                            animation_of_hero = true;
                        }

                        bounds = frg_ic.get_sprite().getGlobalBounds();

                        if (bounds.contains(mouse)) {
                            state = state_forge;
                        }
                    }
                }

                if (animation_of_hero) {
                    update_hero(hero1, effect, blade, sheath, window, time, fireball_list);
                }
    


                update_fireball_list(fireball_list, time, monster, health_zeb);
                window.clear();
                window.draw(fight_background.get_sprite());
                window.draw(monster.get_sprite());
                window.draw(health_zeb.get_sprite());
                window.draw(health_bez.get_sprite());

                std::ostringstream forge_score_str;
                forge_score_str << gold;
                text.setString("" + forge_score_str.str());
                text.setPosition(1020, 0);
                window.draw(text);
                // window.draw(fireball.get_sprite());
                // window.draw(kuznya.get_sprite());

                draw_hero(hero1, sheath, blade, effect, window);
                //draw_kuznya(blacksmith, hammer, sparks, anvil, window);
                draw_fireballs(fireball_list, window, time);
                window.draw(frg_ic.get_sprite());


                window.display();
                break;
            }
        }
    }
}



void TApp::End() {
      
}

