//
//  static_object.cpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 19.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#include "static_object.hpp"
#include "ResourcePath.hpp"

static_object::static_object(
        std::string path_to_file,
        double x,
        double y,
        double width,
        double height)
        :
        path_to_file(path_to_file),
        x(x),
        y(y),
        width(width),
        height(height) {
    image.loadFromFile(resourcePath() + this->path_to_file);
    texture.loadFromImage(image);
    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(0,0, this->width, this->height));
    sprite.setPosition(this->x, this->y);
}

static_object::static_object(const static_object &rhs) {
    path_to_file = rhs.path_to_file;
    x = rhs.x;
    y = rhs.y;
    width = rhs.width;
    height = rhs.height;
    image.loadFromFile(resourcePath() + path_to_file);
    texture.loadFromImage(image);
    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(0,0, this->width, this->height));
    sprite.setPosition(this->x, this->y);
}

static_object &static_object::operator=(const static_object &rhs) {
    path_to_file = rhs.path_to_file;
    x = rhs.x;
    y = rhs.y;
    width = rhs.width;
    height = rhs.height;
    image.loadFromFile(resourcePath() + path_to_file);
    texture.loadFromImage(image);
    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(0,0, this->width, this->height));
    sprite.setPosition(this->x, this->y);
    return *this;
}

sf::Sprite& static_object::get_sprite() {
    return sprite;
}

double static_object::get_x() const {
    return x;
}

double static_object::get_y() const {
    return y;
}

bool static_object::is_dead() const {
    return dead;
}

void static_object::die() {
    dead = true;
}

void static_object::live() {
    dead = false;
}
