//
//  anim_object.cpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 12.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#include "anim_object.hpp"

#include <iostream>

anim_object::anim_object(std::string path_name, double x, double y, double width, double height, int frames)
    : static_object(path_name, x, y, width, height), frames(frames), current_frame(current_frame) {}

anim_object& anim_object::operator=(const anim_object &rhs) {
    static_object::operator=(rhs);
    frames = rhs.frames;
    current_frame = rhs.current_frame;
    return *this;
}

anim_object::anim_object(const anim_object &rhs) : static_object(rhs) {
    frames = rhs.frames;
    current_frame = rhs.current_frame;
}

double anim_object::get_current_frame() const {
    return current_frame;
}

double &anim_object::get_current_frame() {
    return current_frame;
}

int anim_object::get_number_of_frames() const {
    return frames;
}

double anim_object::get_width() const {
    return width;
}

double anim_object::get_height() const {
    return height;
}
