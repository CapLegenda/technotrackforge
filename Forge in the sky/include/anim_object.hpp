//
//  anim_object.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 12.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#pragma once
#include <stdio.h>
#include <SFML/Graphics.hpp>

#include "static_object.hpp"

class anim_object : public static_object {
protected:
    int frames;
    double current_frame = 0;
public:
    anim_object(std::string path_to_file,
                double x,
                double y,
                double width,
                double height,
                int frames);
    anim_object(const anim_object& rhs);
    anim_object& operator=(const anim_object& rhs);

    double get_current_frame() const;
    double &get_current_frame();
    int get_number_of_frames() const;
    double get_width() const;
    double get_height() const;

};
