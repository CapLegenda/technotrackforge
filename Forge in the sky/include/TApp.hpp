//
//  TApp.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 11.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#pragma once

#include "main.hpp"
 
class TApp {
     protected:
        sf::RenderWindow *Window;

     public:
        TApp();
        ~TApp();
        void Init();
        void Run();
        void End();
};

