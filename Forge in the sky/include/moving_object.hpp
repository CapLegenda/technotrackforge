//
//  moving_object.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 19.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#ifndef moving_object_hpp
#define moving_object_hpp

#include <stdio.h>
#include "anim_object.hpp"

class moving_object : public anim_object {
protected:
    double dx = 0;
    double dy = 0;
public:
    moving_object(
            std::string path_name,
            double x,
            double y,
            double width,
            double height,
            int frames,
            double dx,
            double dy);
    moving_object(const moving_object &rhs);
    moving_object operator=(const moving_object &rhs);
    double &get_x();
    double &get_y();

    double &get_dx();
    double &get_dy();
    double get_dx() const;
    double get_dy() const;

};

#endif /* moving_object_hpp */
