//
//  static_object.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 19.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#ifndef static_object_hpp
#define static_object_hpp

#include <stdio.h>

#include <SFML/Graphics.hpp>

class static_object {
protected:
    double x, y, width, height;
    bool dead = true;
    sf::Image image;
    sf::Texture texture;
    sf::Sprite sprite;
    std::string path_to_file;
public:
    static_object(
            std::string path_to_file,
            double x,
            double y,
            double width,
            double height);
    static_object(const static_object& rhs);
    static_object& operator=(const static_object& rhs);
    sf::Sprite& get_sprite();
    double get_x() const;
    double get_y() const;
    bool is_dead() const;
    void die();
    void live();
};


#endif /* static_object_hpp */
