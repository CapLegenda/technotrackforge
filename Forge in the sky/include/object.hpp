//
//  object.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 11.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#pragma once

#include <stdio.h>
#include "ResourcePath.hpp"
#include "main.hpp"

class Object {
 public:
    sf::Texture texture;
    sf::Image image;
    sf::Sprite sprite;

    Object(const std::string file_path, float x, float y) {
        image.loadFromFile(resourcePath() + file_path);
        image.createMaskFromColor(sf::Color(255, 255, 255));
        texture.loadFromImage(image);
        sprite.setTexture(texture);
        sprite.setScale(0.7, 0.7);

        sprite.setPosition(x, y);
    }
};
