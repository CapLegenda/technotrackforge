//
//  main.hpp
//  Forge in the sky
//
//  Created by Магомед Магомедов on 11.05.2020.
//  Copyright © 2020 Магомед Магомедов. All rights reserved.
//

#pragma once

#define SFML_STATIC
//---SFML---
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>


